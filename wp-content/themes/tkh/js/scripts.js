$ = jQuery.noConflict();

$(document).ready(function() {

    $('.first-line').fadeIn(1500);
    $('.dark').fadeIn(1500);

    

    $('#w-czym-pomoc div').on('click', function() {

        if(!$(this).hasClass('active')) {

            $(this).parent().find('.active').removeClass('active');

            $(this).addClass('active');



            var name = $(this).attr('name');

            $('.bar-content').find('.active').fadeOut('slow', function() {

                $(this).removeClass('active');

                $('.' + name).fadeIn('slow', function() {

                    $('.' + name).addClass('active');

                });

                if($(window).width() < 768) {

                    $('body').animate({

                        scrollTop: $('.bars-content').offset().top

                    });

                }

            });

        }

    });



    $('#jak-proces div').on('click', function() {

        if(!$(this).hasClass('active')) {

            $(this).parent().find('.active').removeClass('active');

            $(this).addClass('active');



            var name = $(this).attr('name');



            $('.content.active').fadeOut('slow', function() {

                $(this).removeClass('active');

                $('.content#' + name).fadeIn('slow', function() {

                    $(this).addClass('active');

                });

                if($(window).width() < 768) {

                    $('body').animate({

                        scrollTop: $('.content#' + name).offset().top

                    });

                }

            })

            // $('.bar-content').find('.active').fadeOut('slow', function() {

            //     $(this).removeClass('active');

            //     $('.' + name).fadeIn('slow', function() {

            //         $('.' + name).addClass('active');

            //     })

            // });

        }

    });

    $('.referencje .fa-chevron-right').on('click', function() {
        $('.referencje blockquote').find('.active').fadeOut('fast', function() {
            if(!$(this).is(':last-of-type')) {
                $(this).removeClass('active');
                $(this).next().fadeIn('fast', function() {
                    $(this).addClass('active');
                    var id = $(this).attr('data-id');
                    $('#referencje').find('h3.active').fadeOut('fast', function() {
                        $(this).removeClass('active');
                        $('#referencje #' + id).fadeIn('fast', function() {
                            $(this).addClass('active');
                        });
                    });
                });
            } else {
                $(this).removeClass('active');
                $('blockquote p:first-of-type').fadeIn('fast', function() {
                    $(this).addClass('active');
                    var id = $(this).attr('data-id');
                    $('#referencje').find('h3.active').fadeOut('fast', function() {
                        $(this).removeClass('active');
                        $('#referencje #' + id).fadeIn('fast', function() {
                            $(this).addClass('active');
                        });
                    });
                });
            }
        });
    });

    $('.referencje .fa-chevron-left').on('click', function() {
        $('.referencje blockquote').find('.active').fadeOut('fast', function() {
            if(!$(this).is(':first-of-type')) {
                $(this).removeClass('active');
                $(this).prev().fadeIn('fast', function() {
                    $(this).addClass('active');
                    var id = $(this).attr('data-id');
                    $('#referencje').find('h3.active').fadeOut('fast', function() {
                        $(this).removeClass('active');
                        $('#referencje #' + id).fadeIn('fast', function() {
                            $(this).addClass('active');
                        });
                    });
                });
            } else {
                $(this).removeClass('active');
                $('blockquote p:last-of-type').fadeIn('fast', function() {
                    $(this).addClass('active');
                    var id = $(this).attr('data-id');
                    $('#referencje').find('h3.active').fadeOut('fast', function() {
                        $(this).removeClass('active');
                        $('#referencje #' + id).fadeIn('fast', function() {
                            $(this).addClass('active');
                        });
                    });
                });
            }
        });
    });


});