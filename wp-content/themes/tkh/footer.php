<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
            <img src="<?php echo get_template_directory_uri(); ?>/img/tkhfoot.png" class="img-responsive" alt="TKH">
            </div>
            <div class="col-sm-3">
                <h5>Lorem ipsum</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum odit, placeat? Aperiam iure fugiat, eligendi cupiditate libero esse nisi? Aliquid obcaecati, non sapiente quas tenetur inventore dolorum ea vitae quasi.</p>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
                <h5>Nasz adres</h5>
                <span>ul. Suraska 1 lokal 122</span>
                <br>
                <span>15-093 Białystok</span>
                <h5>Godziny otwarcia</h5>
                <span>Pon.-Pt.: 8:00 - 18:00</span>
                <br>
                <span>Sob.: 9:00 - 15:00</span> 
                <br>
                <span>Nd.: nieczynne</span>
            </div>
            <div class="col-sm-3">
                <h5>Skontaktuj się z nami</h5>
                <span>e-mail: <a href="mailto:tkh_biuro@tkh.com.pl">tkh_biuro@tkh.com.pl</a></span>
                <h5>Znajdziesz nas tutaj:</h5>
            </div>
        </div>
    </div>
</footer>
<div class="stopka">

</div>
<?php wp_footer(); ?>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCroEFEoQDPOc1AVWnBQvX-ytobWaqW-zg&callback=initMap">
    </script>
<script>
function initMap() {
        var uluru = {lat: 53.131589, lng: 23.157193};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru,
          scrollwheel: false,
          navigationControl: false,
          mapTypeControl: false,
          scaleControl: false,
          draggable: true,
          styles: [
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e9e9e9"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dedede"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#333333"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f2f2f2"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    }
]
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          icon: templateUrl + '/img/marker.png'
        });
      }
</script>
</body>
</html>