<?php
function registerMenu() {
    register_nav_menu('main-menu',__( 'Main' ));
}
add_action( 'init', 'registerMenu' );

function mainNav() {
	wp_nav_menu(
	array(
		'theme_location'  => '',
		'menu'            => 'Top Menu',
		'container'       => '',
		'container_class' => '',
		'container_id'    => '',
		'menu_class'      => '',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul class="nav navbar-nav">%3$s</ul>',
		'depth'           => 0,
		'walker'          =>''
		)
	);
}
?>