<?php

/*

    Template name: Strona główna

    Author: Tomasz Misiukiewicz

    Author URI: http://misiukiewicz.eu/

    Version: 1.0

*/

$template = get_template_directory_uri();





$o_mnie_tlo = types_render_field( "o-mnie-tlo", array( "raw" => "true" ) );

$o_mnie_tytul = types_render_field( "o-mnie-tytul", array( "raw" => "true" ) );

$o_mnie_zdjecie = types_render_field( "o-mnie-zdjecie", array( "raw" => "true" ) );

$o_mnie_opis = types_render_field( "o-mnie-opis", array( "raw" => "true" ) );



$doswiadczenie_tlo = types_render_field( "doswiadczenie-tlo", array( "raw" => "true" ) );

$doswiadczenie_tytul = types_render_field( "doswiadczenie-tytul", array( "raw" => "true" ) );

$doswiadczenie_zdjecie = types_render_field( "doswiadczenie-obrazek", array( "raw" => "true" ) );

$doswiadczenie_opis = types_render_field( "doswiadczenie-tekst", array( "raw" => "true" ) );



$niezaleznosc_tlo = types_render_field( "niezaleznosc-tlo", array( "raw" => "true" ) );

$niezaleznosc_tytul = types_render_field( "niezaleznosc-tytul", array( "raw" => "true" ) );

$niezaleznosc_zdjecie = types_render_field( "niezaleznosc-obrazek", array( "raw" => "true" ) );

$niezaleznosc_opis = types_render_field( "niezaleznosc-tekst", array( "raw" => "true" ) );





$hipoteczny_lewa = types_render_field( "kredyt-hipoteczny-lewa", array( "raw" => "true" ) );

$hipoteczny_prawa = types_render_field( "kredyt-hipoteczny-prawa", array( "raw" => "true" ) );

$konsolidacyjny_lewa = types_render_field( "kredyt-konsolidacyjny-lewa", array( "raw" => "true" ) );

$konsolidacyjny_prawa = types_render_field( "kredyt-konsolidacyjny-prawa", array( "raw" => "true" ) );

$pozyczki_lewa = types_render_field( "pozyczki-hipoteczne-lewa", array( "raw" => "true" ) );

$pozyczki_prawa = types_render_field( "pozyczki-hipoteczne-prawa", array( "raw" => "true" ) );

$refinansowe_lewa = types_render_field( "kredyty-refinansowe-lewa", array( "raw" => "true" ) );

$refinansowe_prawa = types_render_field( "kredyty-refinansowe-prawa", array( "raw" => "true" ) );



$wspolpraca_tytul = types_render_field( "wspolpraca-tytul", array( "raw" => "true" ) );

$wspolpraca_tresc = types_render_field( "wspolpraca-tresc", array( "raw" => "true" ) );



$czego_sie_dowiesz_tytul = types_render_field( "czego-sie-dowiesz-tytul", array( "raw" => "true" ) );

$czego_sie_dowiesz_tresc = types_render_field( "czego-sie-dowiesz-tresc", array( "raw" => "true" ) );



$ps = types_render_field( "ps-tekst", array( "raw" => "true" ) );



$kontakt_tekst = types_render_field( "kontakt-tekst", array( "raw" => "true" ) );

$informacja = types_render_field( "informacja", array( "raw" => "true" ) );





/* 1 */





$tlo_11 = types_render_field( "1-1-tlo", array( "raw" => "true" ) );

$tytul_11 = types_render_field( "1-1-tytul", array( "raw" => "true" ) );

$obrazek_11 = types_render_field( "1-1-obrazek", array( "raw" => "true" ) );

$tresc_11 = types_render_field( "1-1-tresc", array( "raw" => "true" ) );



$tlo_12 = types_render_field( "1-2-tlo", array( "raw" => "true" ) );

$tytul_12 = types_render_field( "1-2-tytul", array( "raw" => "true" ) );

$obrazek_12 = types_render_field( "1-2-obrazek", array( "raw" => "true" ) );

$tresc_12 = types_render_field( "1-2-tekst", array( "raw" => "true" ) );        //tu sie rozni



$tlo_13 = types_render_field( "1-3-tlo", array( "raw" => "true" ) );

$tytul_13 = types_render_field( "1-3-tytul", array( "raw" => "true" ) );

$obrazek_13 = types_render_field( "1-3-obrazek", array( "raw" => "true" ) );

$tresc_13 = types_render_field( "1-3-tresc", array( "raw" => "true" ) );



/* 2 */



$tlo_21 = types_render_field( "2-1-tlo", array( "raw" => "true" ) );

$tytul_21 = types_render_field( "2-1-tytul", array( "raw" => "true" ) );

$obrazek_21 = types_render_field( "2-1-obrazek", array( "raw" => "true" ) );

$tresc_21 = types_render_field( "2-1-tresc", array( "raw" => "true" ) );



$tlo_22 = types_render_field( "2-2-tlo", array( "raw" => "true" ) );

$tytul_22 = types_render_field( "2-2-tytul", array( "raw" => "true" ) );

$obrazek_22 = types_render_field( "2-2-obrazek", array( "raw" => "true" ) );

$tresc_22 = types_render_field( "2-2-tekst", array( "raw" => "true" ) );        //tu sie rozni



$tlo_23 = types_render_field( "2-3-tlo", array( "raw" => "true" ) );

$tytul_23 = types_render_field( "2-3-tytul", array( "raw" => "true" ) );

$obrazek_23 = types_render_field( "2-3-obrazek", array( "raw" => "true" ) );

$tresc_23 = types_render_field( "2-3-tresc", array( "raw" => "true" ) );



/* 3 */



$tlo_31 = types_render_field( "3-1-tlo", array( "raw" => "true" ) );

$tytul_31 = types_render_field( "3-1-tytul", array( "raw" => "true" ) );

$obrazek_31 = types_render_field( "3-1-obrazek", array( "raw" => "true" ) );

$tresc_31 = types_render_field( "3-1-tresc", array( "raw" => "true" ) );



$tlo_32 = types_render_field( "3-2-tlo", array( "raw" => "true" ) );

$tytul_32 = types_render_field( "3-2-tytul", array( "raw" => "true" ) );

$obrazek_32 = types_render_field( "3-2-obrazek", array( "raw" => "true" ) );

$tresc_32 = types_render_field( "3-2-tresc", array( "raw" => "true" ) );        //tu sie rozni



$tlo_33 = types_render_field( "3-3-tlo", array( "raw" => "true" ) );

$tytul_33 = types_render_field( "3-3-tytul", array( "raw" => "true" ) );

$obrazek_33 = types_render_field( "3-3-obrazek", array( "raw" => "true" ) );

$tresc_33 = types_render_field( "3-3-tresc", array( "raw" => "true" ) );



/* 4 */



$tlo_41 = types_render_field( "4-1-tlo", array( "raw" => "true" ) );

$tytul_41 = types_render_field( "4-1-tytul", array( "raw" => "true" ) );

$obrazek_41 = types_render_field( "4-1-obrazek", array( "raw" => "true" ) );

$tresc_41 = types_render_field( "4-1-tresc", array( "raw" => "true" ) );



$tlo_42 = types_render_field( "4-2-tlo", array( "raw" => "true" ) );

$tytul_42 = types_render_field( "4-2-tytul", array( "raw" => "true" ) );

$obrazek_42 = types_render_field( "4-2-obrazek", array( "raw" => "true" ) );

$tresc_42 = types_render_field( "4-2-tresc", array( "raw" => "true" ) );        //tu sie rozni



$tlo_43 = types_render_field( "4-3-tlo", array( "raw" => "true" ) );

$tytul_43 = types_render_field( "4-3-tytul", array( "raw" => "true" ) );

$obrazek_43 = types_render_field( "4-3-obrazek", array( "raw" => "true" ) );

$tresc_43 = types_render_field( "4-3-tresc", array( "raw" => "true" ) );



/* 5 */



$tlo_51 = types_render_field( "5-1-tlo", array( "raw" => "true" ) );

$tytul_51 = types_render_field( "5-1-tytul", array( "raw" => "true" ) );

$obrazek_51 = types_render_field( "5-1-obrazek", array( "raw" => "true" ) );

$tresc_51 = types_render_field( "5-1-tresc", array( "raw" => "true" ) );



$tlo_52 = types_render_field( "5-2-tlo", array( "raw" => "true" ) );

$tytul_52 = types_render_field( "5-2-tytul", array( "raw" => "true" ) );

$obrazek_52 = types_render_field( "5-2-obrazek", array( "raw" => "true" ) );

$tresc_52 = types_render_field( "5-2-tresc", array( "raw" => "true" ) );        //tu sie rozni



$tlo_53 = types_render_field( "5-3-tlo", array( "raw" => "true" ) );

$tytul_53 = types_render_field( "5-3-tytul", array( "raw" => "true" ) );

$obrazek_53 = types_render_field( "5-3-obrazek", array( "raw" => "true" ) );

$tresc_53 = types_render_field( "5-3-tresc", array( "raw" => "true" ) );





 get_header(); ?>

<div class="rog">

    <img src="<?php echo $template; ?>/img/rog.png" class="img-responsive">

</div>

<div class="container-fluid navbar">

    <div class="row">

        <div class="menu-container pull-right">

            <div class="navbar-header">

                <a class="navbar-brand" href=""><img src="<?php echo $template;?>/img/logo-simple.png" class="img-responsive"></a>

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">

                    <i class="fa fa-bars" aria-hidden="true"></i>

                </button>

            </div>

            <div class="collapse navbar-collapse" id="myNavbar">

            <?php mainNav(); ?>

            </div>

        </div>

    </div>

</div>

<div class="container-fluid navigation">
    <div class="navbar-header">

                <a class="navbar-brand" href=""><img src="<?php echo $template;?>/img/logo-simple.png" class="img-responsive"></a>

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">

                    <i class="fa fa-bars" aria-hidden="true"></i>

                </button>

            </div>

            <div class="collapse navbar-collapse pull-right" id="myNavbar">

            <?php mainNav(); ?>

            </div>
</div>
<style>
.navigation .navbar-nav li a {
    padding: 15px 10px;
}
.navigation .navbar-brand {
    display: block;
}
.navigation .navbar-brand img {
    width: auto;
    margin-top: -23px;
    max-height: 75px;
}
.navigation {
	padding: 25px 0;
	background-color: white;
	position: -webkit-sticky;
}
.sticky {
  position: fixed;
  width: 100%;
  left: 0;
  top: 0;
  z-index: 100;
  border-top: 0;
}
</style>
<script>
$(document).ready(function() {
    // grab the initial top offset of the navigation 
    var stickyNavTop = $('.navigation').offset().top;
    // our function that decides weather the navigation bar should have "fixed" css position or not.
    var stickyNav = function(){
        var scrollTop = $(window).scrollTop(); // our current vertical position from the top
                
        // if we've scrolled more than the navigation, change its position to fixed to stick to top,
        // otherwise change it back to relative
        if (scrollTop > stickyNavTop) { 
            $('.navigation').addClass('sticky');
            $('.navigation').show();
        } else {
            $('.navigation').removeClass('sticky'); 
            $('.navigation').hide();
        }
    };

    stickyNav();
    // and run it again every time you scroll
    $(window).scroll(function() {
        stickyNav();
    });
		});
</script>
<div class="clearfix"></div>

<div class="container-fluid baner">

    <div class="row absolute first-line">

        <div class="col-md-7 light">

            <h1>Oszczędź pieniądze, czas oraz nerwy</h1>

        </div>

        <div class="col-sm-9 dark pull-right">

            <h1>I weź najlepszy kredyt hipoteczny jaki możesz dostać</h1>

        </div>    

    </div>

</div>

<div id="welcome" class="welcome section <?php if($o_mnie_tlo == 2) echo 'grey-bg'; ?>">

    <div class="container">

        <div class="row">

            <div class="col-sm-4">

                <img src="<?php echo $o_mnie_zdjecie; ?>" class="img-responsive">

            </div>

            <div class="col-sm-8">

                <h2><?php echo $o_mnie_tytul; ?></h2>

                <div class="clearfix"></div>

                <div class="section-content">

                    <?php echo $o_mnie_opis; ?>

                </div>

            </div>

        </div>

    </div>

</div>

<div class="<?php if($doswiadczenie_tlo == 2) echo 'grey-bg'; ?> section">

    <div class="container">

        <div class="row">

            <div class="col-sm-6 col-sm-push-6">

                <img src="<?php echo $doswiadczenie_zdjecie;?>" class="img-responsive pull-right-sm">

            </div>

            <div class="col-sm-6 col-sm-pull-6">

                <h2><?php echo $doswiadczenie_tytul; ?></h2>

                <?php echo $doswiadczenie_opis; ?>

            </div>

        </div>

    </div>

</div>

<div class="niezaleznosc section <?php if($niezaleznosc_tlo == 2) echo 'grey-bg'; ?>">

    <div class="container">

        <div class="row">

            <div class="col-sm-6 hidden-xs">

                <img src="<?php echo $niezaleznosc_zdjecie;?>" class="img-responsive">

            </div>

            <div class="col-sm-6">

                <h2><?php echo $niezaleznosc_tytul; ?></h2>

                <img src="<?php echo $niezaleznosc_zdjecie;?>" class="img-responsive visible-xs">

                <?php echo $niezaleznosc_opis; ?>

            </div>

        </div>

    </div>

</div>

<div class="section referencje" id="referencje">
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h2>Referencje</h2>
            <div class="pull-right arrows">
                <i class="fa fa-chevron-left" aria-hidden="true"></i>
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </div>
        </div>
        <div class="col-xs-12">
        <blockquote>
        <?php
        $references = array(
    			'post_type' => 'referencja',
    			'order' => 'ASC',
    			'posts_per_page' => -1
    		);
    		$referencesQuery = new WP_Query($references);
    		$i = 0;
          if ( $referencesQuery->have_posts() ) {
    				while ( $referencesQuery->have_posts() ) {
    					$referencesQuery->the_post();
    					$postMeta = get_post_meta($post->ID);
    					// $offerTitle = get_post_field( 'post_name', get_post() );
              $title = get_the_title();
              $id = get_the_id();
              $content = get_the_content();
              if($i==0) {
                  echo '<p class="active" data-id="' . $id . '">' . $content . '</p>';
              } else {
                echo '<p data-id="' . $id . '">' . $content . '</p>';
              }
              $i++;
    				}
    			}
         ?>
        </blockquote>
        <?php
        $i = 0;
        if ( $referencesQuery->have_posts() ) {
            while ( $referencesQuery->have_posts() ) {
                $referencesQuery->the_post();
                $postMeta = get_post_meta($post->ID);
                $title = get_the_title();
                $id = get_the_id();              
                if($i==0) {
                    echo '<h3 class="author active" id="' . $id . '">' . $title . '</h3>';
                } else {
                    echo '<h3 class="author" id="' . $id . '">' . $title . '</h3>';
                }
                $i++;
            }
        }
        ?>
        </div>
    </div>
</div>
<style>
.author:not(.active) {
    display: none;
}
</style>
</div>

<div class="pierwsze1 section <?php if($tlo_11 == 2) echo 'grey-bg'; ?>">

    <div class="container">

        <div class="row">

            <div class="col-sm-6">

                <h2><?php echo $tytul_11; ?></h2>

                <img src="<?php echo $obrazek_11; ?>" class="img-responsive visible-xs">

                <?php echo $tresc_11; ?>

            </div>

            <div class="col-sm-6 hidden-xs">

                <img src="<?php echo $obrazek_11; ?>" class="img-responsive">

            </div>

        </div>

    </div>

</div>

<div class="clearfix"></div>

<div class="pierwsze2 section <?php if($tlo_12 == 2) echo 'grey-bg'; ?>">

    <div class="container">

        <div class="row">

            <div class="col-sm-6 hidden-xs">

                <img src="<?php echo $obrazek_12; ?>" class="img-responsive">

            </div>

            <div class="col-sm-6">

                <h2><?php echo $tytul_12; ?></h2>

                <img src="<?php echo $obrazek_12; ?>" class="img-responsive visible-xs">

                <?php echo $tresc_12; ?>

            </div>

        </div>

    </div>

</div>

<div class="clearfix"></div>

<div class="pierwsze3 section <?php if($tlo_13 == 2) echo 'grey-bg'; ?>">

    <div class="container">

        <div class="row">

            <div class="col-sm-6">

                <h2><?php echo $tytul_13; ?></h2>

                <img src="<?php echo $obrazek_13; ?>" class="img-responsive visible-xs">

                <?php echo $tresc_13; ?>

            </div>

            <div class="col-sm-6 hidden-xs">

                <img src="<?php echo $obrazek_13; ?>" class="img-responsive">

            </div>

        </div>

    </div>

</div>

<div class="clearfix"></div>

<div id="pomoc" class="pomoc section">

    <div class="container">

        <div class="row">

            <div class="col-xs-12">

                <h3>W czym mogę pomóc?</h3>

            </div>

            <div class="clearfix"></div>

            <div class="bars" id="w-czym-pomoc">

                <div class="col-sm-3 active" name="kredyt-hipoteczny">Kredyt Hipoteczny</div>

                <div class="col-sm-3" name="kredyt-konsolidacyjny">Kredyty konsolidacyjne</div>

                 <div class="col-sm-3" name="pozyczka">Pożyczki hipoteczne</div>

                 <div class="col-sm-3" name="kredyt-refinansowy">Kredyty refinansowe</div>

            </div>

            

        </div>

    </div>

    <div class="container-fluid bars-content">

        <div class="row">

            <div class="col-sm-6 bar-content">

                <div class="col-sm-9 pull-right">

                <p class="kredyt-hipoteczny active"><?php echo $hipoteczny_lewa; ?></p>

                <p class="kredyt-konsolidacyjny"><?php echo $konsolidacyjny_lewa; ?></p>

                <p class="pozyczka"><?php echo $pozyczki_lewa; ?></p>

                <p class="kredyt-refinansowy"><?php echo $refinansowe_lewa; ?></p>

                </div>

            </div>

            <div class="col-sm-6 bar-content">

                <div class="col-sm-9">

                <p class="kredyt-hipoteczny active"><?php echo $hipoteczny_prawa; ?></p>

                <p class="kredyt-konsolidacyjny"><?php echo $konsolidacyjny_prawa; ?></p>

                <p class="pozyczka"><?php echo $pozyczki_prawa; ?></p>

                <p class="kredyt-refinansowy"><?php echo $refinansowe_prawa; ?></p>

                </div>

            </div>

        </div>

    </div>

</div>

<div class="pierwsze2 section <?php if($tlo_21 == 2) echo 'grey-bg'; ?>">

    <div class="container">

        <div class="row">

            <div class="col-sm-6 hidden-xs">

                <img src="<?php echo $obrazek_21;?>" class="img-responsive">

            </div>

            <div class="col-sm-6">

                <h2><?php echo $tytul_21;?></h2>

                <img src="<?php echo $obrazek_21;?>" class="img-responsive visible-xs">

                <?php echo $tresc_21;?>

            </div>

        </div>

    </div>

</div>

<div class="clearfix"></div>

<div class="pierwsze1 section <?php if($tlo_22 == 2) echo 'grey-bg'; ?>">

    <div class="container">

        <div class="row">

            <div class="col-sm-6">

                <h2><?php echo $tytul_22;?></h2>

                <img src="<?php echo $obrazek_22;?>" class="img-responsive visible-xs">

                <?php echo $tresc_22;?>

            </div>

            <div class="col-sm-6 hidden-xs">

                <img src="<?php echo $obrazek_22;?>" class="img-responsive">

            </div>

        </div>

    </div>

</div>

<div class="clearfix"></div>

<div class="pierwsze2 section <?php if($tlo_23 == 2) echo 'grey-bg'; ?>">

    <div class="container">

        <div class="row">

            <div class="col-sm-6 hidden-xs">

                <img src="<?php echo $obrazek_23;?>" class="img-responsive">

            </div>

            <div class="col-sm-6">

                <h2><?php echo $tytul_23; ?></h2>

                <img src="<?php echo $obrazek_23;?>" class="img-responsive visible-xs">

                <?php echo $tresc_23; ?>

            </div>

        </div>

    </div>

</div>

<div class="clearfix"></div>

<div id="jak-wyglada-proces" class="section jak-wyglada-proces">

    <div class="container">

        <div class="row">

            <div class="col-xs-12">

                <h3 style="margin-top: 0;">Jak wygląda proces?</h3>

            </div>

            <div class="col-xs-12">

                <?php

                    $bars = array(

                        'zakup-mieszkania' => 'Zakup mieszkania',

                        'budowa-domu' => 'Budowa domu',

                        'konsolidacja' => 'Konsolidacja',

                        'pozyczka-hipoteczna' => 'Pożyczka hipoteczna'

                    );

                 ?>

                <div class="bars" id="jak-proces">

                    <?php

                    $i = 0;

                    foreach($bars as $key => $value) {

                        if($i == 0) {

                            echo '<div class="col-sm-3 active" name="' . $key .'">' . $value . '</div>';



                        } else {

                            echo '<div class="col-sm-3" name="' . $key .'">' . $value . '</div>';

                        }

                        $i++;

                    } ?>

                </div>

                <div class="clearfix"></div>

                <?php

                if(have_rows('proces')) {

                ?>

                <?php

                $j = 0; 

                foreach($bars as $key => $value) { 

                    $k = 1;                    

                ?>

                <div class="content <?php if($j==0) echo 'active'; ?>" id="<?php echo $key; ?>">

                <?php

                 while(have_rows('proces')) {

                    $position = the_row();

                    $kategoria = get_sub_field('kategoria');

                    if($position['kategoria'] == $key) {

                        if($k == 1 || $k == 5) {

                            echo '<div class="row rzad">';

                        }

                        echo 

                        '<div class="col-sm-3 element">

                            <img src="' . $position['ikona'] . '" class="img-responsive">

                            <span>' . $k . '</span>

                            <h5>' . $position['tytul'] . '</h5>

                            <h4>' . $position['tekst_po_najechaniu'] . '</h4>

                        </div>';

                        if($k == 4 || $k == 8) {

                            echo '</div>';

                        }

                        $k++;

                    }

                }

                if($k != 4 || $k != 8) {

                    echo '</div>';

                }

                ?>

                </div>

                <div class="clearfix"></div>                

                <?php $j++; } ?>

                <?php

                } 

                ?>

            </div>

        </div>

    </div>

</div>

<div class="pierwsze1 section <?php if($tlo_31 == 2) echo 'grey-bg'; ?>">

<div class="container">

    <div class="row">

        <div class="col-sm-6">

            <h2><?php echo $tytul_31; ?></h2>

            <img src="<?php echo $obrazek_31; ?>" class="img-responsive visible-xs">

            <?php echo $tresc_31; ?>

        </div>

        <div class="col-sm-6 hidden-xs">

            <img src="<?php echo $obrazek_31; ?>" class="img-responsive">

        </div>

    </div>

</div>

</div>

<div class="clearfix"></div>

<div class="pierwsze2 section <?php if($tlo_32 == 2) echo 'grey-bg'; ?>">

<div class="container">

    <div class="row">

        <div class="col-sm-6 hidden-xs">

            <img src="<?php echo $obrazek_32; ?>" class="img-responsive">

        </div>

        <div class="col-sm-6">

            <h2><?php echo $tytul_32; ?></h2>

            <img src="<?php echo $obrazek_32; ?>" class="img-responsive visible-xs">

            <?php echo $tresc_32; ?>

        </div>

    </div>

</div>

</div>

<div class="clearfix"></div>

<div class="pierwsze3 section <?php if($tlo_33 == 2) echo 'grey-bg'; ?>">

<div class="container">

    <div class="row">

        <div class="col-sm-6">

            <h2><?php echo $tytul_33; ?></h2>

            <img src="<?php echo $obrazek_33; ?>" class="img-responsive visible-xs">

            <?php echo $tresc_33; ?>

        </div>

        <div class="col-sm-6 hidden-xs">

            <img src="<?php echo $obrazek_33; ?>" class="img-responsive">

        </div>

    </div>

</div>

</div>

<div class="clearfix"></div>

<div id="wspolpraca" class="section wspolpraca">

    <div class="container">

        <div class="row">

            <div class="col-sm-5 pull-right">

                <h2><?php echo $wspolpraca_tytul; ?></h2>

                <?php echo $wspolpraca_tresc; ?>

            </div>

        </div>

    </div>

    <div class="pamietaj-ze">

        <div>

            <h4>Pamiętaj że:</h4>

        </div>

        <div>

            <p>1. Moje doradztwo jest bezpłatne na każdym etapie procesu</p>

            <p>2. Oferty takie same jak w banku</p>

        </div>

    </div>

</div>

<div class="pierwsze2 section <?php if($tlo_41 == 2) echo 'grey-bg'; ?>">

<div class="container">

    <div class="row">

        <div class="col-sm-6 hidden-xs">

            <img src="<?php echo $obrazek_41;?>" class="img-responsive">

        </div>

        <div class="col-sm-6">

            <h2><?php echo $tytul_41;?></h2>

            <img src="<?php echo $obrazek_41;?>" class="img-responsive visible-xs">

            <?php echo $tresc_41;?>

        </div>

    </div>

</div>

</div>

<div class="clearfix"></div>

<div class="pierwsze1 section <?php if($tlo_42 == 2) echo 'grey-bg'; ?>">

<div class="container">

    <div class="row">

        <div class="col-sm-6">

            <h2><?php echo $tytul_42;?></h2>

            <img src="<?php echo $obrazek_42;?>" class="img-responsive visible-xs">

            <?php echo $tresc_42;?>

        </div>

        <div class="col-sm-6 hidden-xs">

            <img src="<?php echo $obrazek_42;?>" class="img-responsive">

        </div>

    </div>

</div>

</div>

<div class="clearfix"></div>

<div class="pierwsze2 section <?php if($tlo_43 == 2) echo 'grey-bg'; ?>">

<div class="container">

    <div class="row">

        <div class="col-sm-6 hidden-xs">

            <img src="<?php echo $obrazek_43;?>" class="img-responsive">

        </div>

        <div class="col-sm-6">

            <h2><?php echo $tytul_43; ?></h2>

            <img src="<?php echo $obrazek_43;?>" class="img-responsive visible-xs">

            <?php echo $tresc_43; ?>

        </div>

    </div>

</div>

</div>

<div class="clearfix"></div>

<div id="pierwsze-spotkanie" class="section proces">

    <div class="container">

        <div class="row">

            <div class="col-md-6 col-sm-12">

            <h2><?php echo $czego_sie_dowiesz_tytul; ?></h2>

            <?php echo $czego_sie_dowiesz_tresc; ?>

            </div>

        </div>

    </div>

</div>

<div class="pierwsze1 section <?php if($tlo_51 == 2) echo 'grey-bg'; ?>">

<div class="container">

    <div class="row">

        <div class="col-sm-6">

            <h2><?php echo $tytul_51; ?></h2>

            <img src="<?php echo $obrazek_51; ?>" class="img-responsive visible-xs">

            <?php echo $tresc_51; ?>

        </div>

        <div class="col-sm-6 hidden-xs">

            <img src="<?php echo $obrazek_51; ?>" class="img-responsive">

        </div>

    </div>

</div>

</div>

<div class="clearfix"></div>

<div class="pierwsze2 section <?php if($tlo_52 == 2) echo 'grey-bg'; ?>">

<div class="container">

    <div class="row">

        <div class="col-sm-6 hidden-xs">

            <img src="<?php echo $obrazek_52; ?>" class="img-responsive">

        </div>

        <div class="col-sm-6">

            <h2><?php echo $tytul_52; ?></h2>

            <img src="<?php echo $obrazek_52; ?>" class="img-responsive visible-xs">

            <?php echo $tresc_52; ?>

        </div>

    </div>

</div>

</div>

<div class="clearfix"></div>

<div class="pierwsze3 section <?php if($tlo_53 == 2) echo 'grey-bg'; ?>" style="padding-bottom: 0;">

<div class="container">

    <div class="row">

        <div class="col-sm-6">

            <h2><?php echo $tytul_53; ?></h2>

            <img src="<?php echo $obrazek_53; ?>" class="img-responsive visible-xs">

            <?php echo $tresc_53; ?>

        </div>

        <div class="col-sm-6 hidden-xs">

            <img src="<?php echo $obrazek_53; ?>" class="img-responsive">

        </div>

    </div>

</div>

<div class="clearfix"></div>

<div class="section ps">

    <div class="container">

        <div class="row">

            <div class="col-sm-4">

                <h1>PS</h1>

            </div>

            <div class="col-sm-8">

                <p><?php echo $ps; ?></p>

            </div>

        </div>

    </div>

</div>

<div id="kontakt" class="form-contact section">

    <div class="container">

        <div class="row">

            <div class="col-sm-5">

                <h2>Napisz do nas</h2>

                <p><?php echo $kontakt_tekst; ?>.</p>

            </div>

            <div class="col-sm-7">

                <?php echo do_shortcode('[contact-form-7 id="15" title="Formularz 1"]'); ?>

            </div>

        </div>

    </div>

</div>

<div class="skype section">

    <div class="container">

        <div class="row">

            <div class="col-sm-5">

                <h2>Informacja dla osób<br>spoza Białegostoku</h2>

                <img src="<?php echo $template; ?>/img/skype.png" class="img-responsive visible-xs">

                <?php echo $informacja; ?>

            </div>

            <div class="col-sm-7 hidden-xs">

                <img src="<?php echo $template; ?>/img/skype.png" class="img-responsive">

            </div>

        </div>

    </div>

</div>

<div id="map"></div>

<?php get_footer(); ?>