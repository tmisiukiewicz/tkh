<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tkh');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Rq-/1/gq`.`}>7[8gOWmJDb{9[x5][,J&ZV? DPMN(s__7~(<9Pn=RnB?9d_!+Jo');
define('SECURE_AUTH_KEY',  '#f!u:UV[/;@T9m(8WqGR6+vrzI@3b|I;odxrUA4Z}pKS3<KD.{X.~Fa9olxe,hX[');
define('LOGGED_IN_KEY',    'UmA%7*@(5&|FwzN}+|YV>bG/Wc(8-#Zv#*QqAx^5gZ_1),=)SIGFhRIB/4%OO&%8');
define('NONCE_KEY',        's!fwAqT%tr!fBIYJ0sW>,w]j4&>#nNxiI<->|YI<Z^+@J+O4Lm7oV;x0F/3T!)nk');
define('AUTH_SALT',        'e{lsZy_}FGL>C2aW t~7y>^#6nR&brfqXcmkLSbRj)x,dJxynB_w[oYRDg<R09bF');
define('SECURE_AUTH_SALT', '`dlH1()kN8Yl4M]7QJkdOt_9j~eVq!IjDh*eI#Z+V Y(hAT<G#nl:*o:*3@|M1gD');
define('LOGGED_IN_SALT',   'PxYFHUdn)~P$.,`PXz+=L-GY^Z5S]Tq {(3}(?^vWC5-(~Li+=g]wJ-@!Z%oWj:,');
define('NONCE_SALT',       'yr/#k pcqPFY[ava2#^7+~Mp&42E8?CpDPq%=0{P(5 G,+HC^PQ?~f4(/yBJ6x2?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
